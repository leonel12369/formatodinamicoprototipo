package com.example.demo.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.spire.pdf.PdfDocument;


@Controller
public class Index {

	 @Autowired
    ServletContext servletContext;

    private final TemplateEngine templateEngine;

    public Index(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }
	    
  
    
	@GetMapping("/")
	public String index() {
		return "view/editor";
	}
	
	@PostMapping("/upload") 
	  public ResponseEntity<?> handleFileUpload( @RequestParam("file") MultipartFile file ) {

	    String fileName = file.getOriginalFilename();
	    try {
	      file.transferTo( new File("D:\\prueba\\" + fileName));
	    } catch (Exception e) {
	      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
	    } 
	    return ResponseEntity.ok("File uploaded successfully.");
	  }
	
	
	@RequestMapping(path = "/pdf")
	public ResponseEntity<?> getPDF(HttpServletRequest request, HttpServletResponse response) throws IOException {

		
	
	    /* Create HTML using Thymeleaf template Engine */

	    WebContext context = new WebContext(request, response, servletContext);
	    
	   // Path path = Paths.get("c:/dev/licenses/windows/readme.txt");
	    
	    String orderHtml = templateEngine.process("foo", context); 

	    /* Setup Source and target I/O streams */

	    ByteArrayOutputStream target = new ByteArrayOutputStream();

	    /*Setup converter properties. */
	    ConverterProperties converterProperties = new ConverterProperties();
	    //converterProperties.setBaseUri("http://localhost:8080");

	    /* Call convert method */
	    HtmlConverter.convertToPdf(orderHtml, target, converterProperties);  

	    /* extract output as bytes */
	    byte[] bytes = target.toByteArray();


	    /* Send the response as downloadable PDF */

	    return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=order.pdf")
                .contentType(MediaType.APPLICATION_PDF)
                .body(bytes);

	}
	
	
	
	
}
