package com.example.demo;

import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.FileTemplateResolver;
import org.springframework.context.annotation.Configuration;

@Configuration
public class configuration {
	
	  @Bean
	    public SpringResourceTemplateResolver firstTemplateResolver() {
	        SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
	        templateResolver.setPrefix("classpath:/templates/");
	        templateResolver.setSuffix(".html");
	        templateResolver.setTemplateMode(TemplateMode.HTML);
	        templateResolver.setCharacterEncoding("UTF-8");
	        templateResolver.setOrder(0);
	        templateResolver.setCheckExistence(true);

	        return templateResolver;
	    }

	    @Bean
	    public FileTemplateResolver secondTemplateResolver() throws MalformedURLException {
	    	
	    	FileTemplateResolver templateResolver = new FileTemplateResolver();
	        templateResolver.setPrefix("D:/prueba/");
	        templateResolver.setSuffix(".html");
	        templateResolver.setTemplateMode(TemplateMode.HTML);
	        templateResolver.setCharacterEncoding("UTF-8");
	        templateResolver.setOrder(1);
	        templateResolver.setCheckExistence(true);

	        return templateResolver;
	    }
}
